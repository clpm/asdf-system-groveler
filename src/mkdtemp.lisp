;;;; Creating temporary directories.
;;;;
;;;; This software is part of asdf-system-groveler. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:asdf-system-groveler/mkdtemp
    (:use #:cl
          #:cffi
          #:uuid)
  (:export #:mkdtemp))

(in-package #:asdf-system-groveler/mkdtemp)

#-windows
(defcfun (%mkdtemp "mkdtemp") :string
  (template :string))

#-windows
(defun mkdtemp (prefix)
  "Make a temporary directory and return its pathname."
  (uiop:ensure-directory-pathname
   (%mkdtemp (uiop:strcat (namestring prefix) "XXXXXX"))))

#+windows
(defun mkdtemp (prefix)
  "Make a temporary directory and return its pathname."
  ;; This is not a secure implementation, but it at least gives us a fallback on
  ;; Windows.
  (let ((pn (format nil "~A~A/" (namestring prefix) (make-v4-uuid))))
    (ensure-directories-exist pn)
    pn))
