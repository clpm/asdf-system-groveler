;;;; The groveler object.
;;;;
;;;; This software is part of asdf-system-groveler. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:asdf-system-groveler/groveler
    (:use #:cl
          #:alexandria
          #:anaphora
          #:asdf-system-groveler/mkdtemp
          #:asdf-system-groveler/sublisp)
  (:export #:groveler-dependency-missing
           #:groveler-dependency-missing-system
           #:groveler-destroy
           #:groveler-add-asd-and-retry
           #:groveler-ensure-asd-loaded
           #:groveler-load-asd
           #:groveler-loaded-asds
           #:groveler-system-info
           #:groveler-systems-in-file
           #:groveler-unknown-error
           #:groveler-unknown-error-backtrace
           #:make-groveler))

(in-package #:asdf-system-groveler/groveler)

(defvar *helper-script* nil
  "The contents of helper.lisp. Evaluated in the sub lisp to give helper
functions.")
(defun read-helper-file-as-string ()
  (read-file-into-string (asdf:system-relative-pathname :asdf-system-groveler "src/helper.lisp")))
(setf *helper-script* (read-helper-file-as-string))

(define-condition groveler-unknown-error ()
  ((backtrace
    :initarg :backtrace
    :reader groveler-unknown-error-backtrace))
  (:report (lambda (c s)
             (format s "Unknown error while groveling. Perhaps your asd file is malformed?~%Backtrace: ~A"
                     (groveler-unknown-error-backtrace c))))
  (:documentation
   "Condition signaled when the groveler encounters an unknown error."))

(define-condition groveler-dependency-missing ()
  ((system
    :initarg :system
    :reader groveler-dependency-missing-system
    :documentation
    "Contains the missing dependency. May be a string or a list."))
  (:report (lambda (c s)
             (format s "Missing dependency: ~S" (groveler-dependency-missing-system c))))
  (:documentation
   "Condition signaled when the groveler cannot continue because of a missing
dependency."))

(defclass groveler ()
  ((sub-lisp
    :initarg :sub-lisp
    :reader groveler-sub-lisp)
   (loaded-asds
    :initform nil
    :initarg :loaded-asds
    :accessor groveler-loaded-asds)
   (killed-p
    :initform nil
    :accessor groveler-killed-p)

   (asdf-pathname
    :initarg :asdf-pathname
    :initform nil
    :accessor groveler-asdf-pathname)
   (asdf-preserve-configuration
    :initarg :asdf-preserve-configuration
    :reader groveler-asdf-preserve-configuration)
   (asdf-allow-self-upgrade
    :initarg :asdf-allow-self-upgrade
    :reader groveler-asdf-allow-self-upgrade)
   (asdf-fasl-cache-dir
    :initarg :asdf-fasl-cache-dir
    :reader groveler-asdf-fasl-cache-dir)
   (keep-asdf-fasl-cache-dir
    :initarg :keep-asdf-fasl-cache-dir
    :reader groveler-keep-asdf-fasl-cache-dir)))

(defgeneric groveler-init (groveler))

(defmethod groveler-init ((groveler groveler))
  (let* ((sub-lisp (groveler-sub-lisp groveler))
         (input (sub-lisp-input sub-lisp))
         (output (sub-lisp-output sub-lisp)))
    (uiop:with-safe-io-syntax ()
      ;; Load ASDF
      (aif (groveler-asdf-pathname groveler)
           (format input "(when (null (find-package '#:asdf)) (load ~S :verbose nil :print nil))~%" (uiop:native-namestring it))
           (format input "(require :asdf)~%"))
      ;; Nuke any existing ASDF configuration.
      (unless (groveler-asdf-preserve-configuration groveler)
        (format input "(asdf:clear-configuration)~%")
        (format input "(setf asdf:*default-source-registries* nil)~%"))
      ;; Set the output translations:
      (format input "(asdf:clear-output-translations)~%")
      (format input "(asdf:initialize-output-translations '(:output-translations :ignore-inherited-configuration (t (~S :implementation :**/ :*.*.*))))~%" (groveler-asdf-fasl-cache-dir groveler))
      ;; Prevent self upgrades.
      (unless (groveler-asdf-allow-self-upgrade groveler)
        (format input "(defun upgrade-asdf () nil)~%"))
      ;; Load the helper functions
      (write-string *helper-script* input)
      (terpri input)
      ;; Start the custom REPL:
      (format input "(asdf-system-groveler/helper:start-repl)~%")
      ;; Print a message so we know when everything has finished:
      (format input "\"asdf-system-groveler ready\"~%")
      (finish-output input)
      ;; Now read lines from the sub lisp until our message is seen.
      (loop
        :for line := (read-line output)
        :until (equal line "\"asdf-system-groveler ready\" "))))
  groveler)

(defun make-groveler (implementation-type
                      &key
                        asdf-pathname
                        asdf-preserve-configuration
                        asdf-allow-self-upgrade
                        (asdf-fasl-cache-dir
                         (mkdtemp (merge-pathnames "asdf-system-groveler"
                                                   (uiop:temporary-directory)))
                         asdf-fasl-cache-dir-provided-p)
                        (keep-asdf-fasl-cache-dir asdf-fasl-cache-dir-provided-p)
                        launch-program-args
                        lisp-path
                        lisp-args
                        (rewrite-arguments-callback #'identity))
  "Return a new GROVELER instance that is attached to another Lisp process and
is ready to have .asd files loaded into it and queried.

IMPLEMENTATION-TYPE is a symbol naming a Lisp implementation known to the
lisp-invocation library or :CUSTOM (in which case, lisp-invocation is not
consulted to provide the list of arguments).

If ASDF-PATHNAME is provided, the file it points to is LOADED into the Lisp
process to provide ASDF. Otherwise, (REQUIRE :ASDF) is used instead.

If ASDF-ALLOW-SELF-UPGRADE is NIL, then ASDF is not allowed to update
itself (which it attempts to do on every invocation of ASDF:OPERATE).

If ASDF-PRESERVE-CONFIGURATION is NIL, then ASDF's configuration is cleared and
its default source registries are cleared. This prevents ASDF from picking up
configuration from environment variables or files.

ASDF-FASL-CACHE-DIR must be a directory pathname where fasls will be stored. It
defaults to a newly created directory for this specific groveler.

If KEEP-ASDF-FASL-CACHE-DIR is NIL, the fasl cache directory will be deleted
when the groveler exits. Defaults to NIL iff ASDF-FASL-CACHE-DIR is not
provided.

LAUNCH-PROGRAM-ARGS is a plist that is provided to UIOP:LAUNCH-PROGRAM when
starting the new Lisp process. This allows you to set things such as environment
variables for the new process.

LISP-PATH is the path to the lisp executable if you do not want the default.

LISP-ARGS is a list of arguments prepended to the arguments provided by the
lisp-invocation library.

If REWRITE-ARGUMENTS-CALLBACK is provided, it is called with the complete list
of arguments and must return the actual list of arguments to use when invoking
the sublisp. This allows you to, for instance, execute the sub lisp in a
sandboxed environment since it may end up running untrusted code.

The returned groveler *must* be destroyed with GROVELER-DESTROY to free
resrouces."
  (let* ((sub-lisp (launch-sub-lisp implementation-type
                                    :rewrite-arguments-callback rewrite-arguments-callback
                                    :launch-program-args launch-program-args
                                    :lisp-path lisp-path
                                    :lisp-args lisp-args))
         (groveler (make-instance 'groveler
                                  :sub-lisp sub-lisp
                                  :asdf-fasl-cache-dir asdf-fasl-cache-dir
                                  :asdf-allow-self-upgrade asdf-allow-self-upgrade
                                  :asdf-preserve-configuration asdf-preserve-configuration
                                  :asdf-pathname asdf-pathname
                                  :keep-asdf-fasl-cache-dir keep-asdf-fasl-cache-dir)))
    (groveler-init groveler)
    groveler))

(defgeneric groveler-destroy (groveler)
  (:documentation
   "Destroy the GROVELER. This must be called in order to free resources used by
the process."))

(defgeneric groveler-ensure-asd-loaded (groveler asd-pathname)
  (:documentation
   "Load the asd file located at ASD-PATHNAME into the GROVELER only if it
hasn't been loaded already. Returns NIL if the asd was already loaded."))

(defgeneric groveler-load-asd (groveler asd-pathname)
  (:documentation
   "Load the asd file located at ASD-PATHNAME into the GROVELER. May signal
GROVELER-UNKNOWN-ERROR or GROVELER-DEPENDENCY-MISSING. Established a restart
named GROVELER-ADD-ASD-AND-RETRY around dependency missing conditions."))

(defgeneric groveler-system-info (groveler system-name)
  (:documentation
   "Returns a plist containing information about the system with name
SYSTEM-NAME from the GROVELER. The plist may contain:

+ :VERSION :: The version of the system.
+ :LICENSE :: The system license.
+ :DESCRIPTION :: The system description.
+ :SOURCE-FILE :: The pathname to the .asd file defining the system.
+ :DEFSYSTEM-DEPENDS-ON :: A list of defsystem dependencies.
+ :DEPENDS-ON :: A list of dependencies for the system.
+ :LOADED-SYSTEMS :: The list of systems loaded when the corresponding asd file
was loaded."))

(defgeneric groveler-systems-in-file (groveler asd-pathname)
  (:documentation
   "Returns a list of system names for systems defined in the .asd file at
ASD-PATHNAME from GROVELER."))

(defun groveler-add-asd-and-retry (asd-pathname &optional callback condition)
  "Invoke the GROVELER-ADD-ASD-AND-RETRY restart. ASD-PATHNAME is the pathname
to an ASD file to add (via GROVELER-LOAD-ASD). If non-NIL, CALLBACK is a
function of no arguments called after the ASD has been successfully loaded."
  (invoke-restart (find-restart 'groveler-add-asd-and-retry condition) asd-pathname callback))

(defmethod groveler-destroy ((groveler groveler))
  (unless (groveler-killed-p groveler)
    (let ((sub-lisp (groveler-sub-lisp groveler)))
      ;; This is not ideal... It'd be nice to actually send the quit form, but I'm
      ;; not confident the sub process will always be in a state it'd be able to
      ;; accept that.
      (multiple-value-prog1 (stop-sub-lisp sub-lisp :signal t :urgent t)
        (setf (groveler-killed-p groveler) t)
        (unless (groveler-keep-asdf-fasl-cache-dir groveler)
          (uiop:delete-directory-tree (groveler-asdf-fasl-cache-dir groveler)
                                      :validate t
                                      :if-does-not-exist :ignore))))))

(defmethod groveler-ensure-asd-loaded ((groveler groveler) asd-pathname)
  (let ((asd-pathname (uiop:ensure-absolute-pathname asd-pathname)))
    (unless (member (namestring asd-pathname) (groveler-loaded-asds groveler) :test #'equal)
      (groveler-load-asd groveler asd-pathname))))

(defmethod groveler-load-asd ((groveler groveler) asd-pathname)
  (let* ((sub-lisp (groveler-sub-lisp groveler))
         (input (sub-lisp-input sub-lisp))
         (output (sub-lisp-output sub-lisp))
         (asd-pathname (uiop:ensure-absolute-pathname asd-pathname)))
    (tagbody
     start
       (uiop:with-safe-io-syntax ()
         (format input "(multiple-value-list (asdf-system-groveler/helper:safe-load-asd ~S))~%"
                 asd-pathname)
         (finish-output input)
         (multiple-value-bind (success-p unknown-error-backtrace missing-system)
             (values-list (read output))
           (unless success-p
             (when (or (null missing-system) unknown-error-backtrace)
               ;; Uh-oh, something happened that we don't know how to recover
               ;; from here. Signal an error!
               (error 'groveler-unknown-error
                      :backtrace unknown-error-backtrace))
             ;; We can provide a way to recover from this!
             (restart-case
                 (error 'groveler-dependency-missing :system missing-system)
               (groveler-add-asd-and-retry (asd &optional callback)
                 :report
                 "Add an asd file to load and try again."
                 :interactive read-asd-path
                 (groveler-load-asd groveler asd)
                 (when callback
                   (funcall callback))
                 (go start))))
           (push (namestring asd-pathname) (groveler-loaded-asds groveler)))))
    groveler))

(defmethod groveler-system-info ((groveler groveler) system-name)
  "Query and return a list of the dependencies of ~system-name~."
  (let* ((sub-lisp (groveler-sub-lisp groveler))
         (input (sub-lisp-input sub-lisp))
         (output (sub-lisp-output sub-lisp)))
    (uiop:with-safe-io-syntax ()
      (format input "(asdf-system-groveler/helper:system-info ~S)~%"
              system-name)
      (finish-output input)
      (read output))))

(defmethod groveler-systems-in-file ((groveler groveler) asd-pathname)
  "Return a list of systems defined in the provided asd file."
  (let* ((sub-lisp (groveler-sub-lisp groveler))
         (input (sub-lisp-input sub-lisp))
         (output (sub-lisp-output sub-lisp))
         (asd-pathname (uiop:ensure-absolute-pathname asd-pathname)))
    (block nil
      (tagbody
       start
         (uiop:with-safe-io-syntax ()
           (format input "(multiple-value-list (asdf-system-groveler/helper:asd-file-systems ~S))~%"
                   asd-pathname)
           (finish-output input)
           (let ((raw-form (read output)))
             (multiple-value-bind (result unknown-error-backtrace missing-system)
                 (values-list raw-form)
               (when unknown-error-backtrace
                 ;; Uh-oh, something happened that we don't know how to recover from
                 ;; here. Signal an error!
                 (error 'groveler-unknown-error
                        :backtrace unknown-error-backtrace))
               (when missing-system
                 ;; We can provide a way to recover from this!
                 (restart-case
                     (error 'groveler-dependency-missing :system missing-system)
                   (groveler-add-asd-and-retry (asd &optional callback)
                     :report
                     "Add an asd file to load and try again."
                     :interactive read-asd-path
                     (groveler-load-asd groveler asd)
                     (when callback
                       (funcall callback))
                     (go start))))
               (return result))))))))

(defun read-asd-path ()
  (format t "Enter a path to an asd file (not evaluated): ")
  (list (read-line)))
