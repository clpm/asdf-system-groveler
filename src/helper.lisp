;;;; ASDF System Groveler Helper
;;;;
;;;; This software is part of asdf-system-grovler. See README.org for more
;;;; information. See LICENSE for license information.
;;;;
;;;; This file is written into the sub lisp to provide helper functions. This
;;;; file is read as a string when the system is loaded.

(uiop:define-package #:asdf-system-groveler/helper
    (:use #:cl)
  (:export #:asd-file-systems
           #:safe-load-asd
           #:start-repl
           #:system-info))

(in-package #:asdf-system-groveler/helper)

(defvar *directly-loaded-systems* (make-hash-table :test 'equal)
  "Maps namestrings (to .asd files) to a list of systems loaded when that .asd
file is ASDF:LOAD-ASD'ed.")

(defvar *file-being-loaded* nil
  "Bound to the namestring of the asd file currently being ASDF:LOAD-ASD'ed")

(defmethod asdf:operate :around ((op asdf:load-op) c &key &allow-other-keys)
  "Intercept calls to load systems so we can record which systems are loaded
when .asd files are loaded via ASDF:LOAD-ASD."
  (if (or (null *file-being-loaded*)
          ;; Ignore ASDF's self upgrades.
          (equalp "asdf" (asdf:component-name c)))
      (call-next-method)
      (progn
        (pushnew (asdf:component-name c) (gethash *file-being-loaded* *directly-loaded-systems*)
                 :test 'equalp)
        (let ((*file-being-loaded* nil))
          (call-next-method)))))

(defun call-with-error-handling (thunk)
  "Call THUNK in an environment where errors are caught. Returns three
VALUES. The first is the result of THUNK or NIL if an error was caught. The
second is a backtrace if an unknown error occurred. The third is the missing
component if an ASDF:MISSING-COMPONENT error is signalled."
  (handler-bind ((error (lambda (c)
                          (return-from call-with-error-handling
                            (values nil (with-output-to-string (s)
                                          (uiop:print-condition-backtrace c :stream s))
                                    nil)))))
    (handler-case
        (values (funcall thunk) nil nil)
      (asdf:missing-component-of-version (c)
        (values nil nil (list :version
                              (string-downcase (string (asdf/find-component:missing-requires c)))
                              (asdf/find-component:missing-version c))))
      (asdf:missing-component (c)
        (values nil nil (string-downcase (string (asdf/find-component:missing-requires c))))))))

(defmacro with-error-handling (nil &body body)
  `(call-with-error-handling (lambda () ,@body)))

(defun safe-load-asd (pathname)
  "Attempt to load the asd file located at PATHNAME. Returns three values, the
first is T iff the file was loaded successfully. The second value is a backtrace
if an unknown error occurred, the third is a missing system."
  (with-error-handling ()
    (let ((*standard-output* (make-broadcast-stream))
          (*error-output* (make-broadcast-stream))
          (*terminal-io* (make-broadcast-stream))
          (*file-being-loaded* (namestring pathname)))
      (asdf:load-asd pathname)
      t)))

(defun system-info (system-name)
  "Given a system name, returns a plist containing information about the
system. Includes:

+ :VERSION :: The version of the system.
+ :LICENSE :: The system license.
+ :DESCRIPTION :: The system description.
+ :SOURCE-FILE :: The pathname to the .asd file defining the system.
+ :DEFSYSTEM-DEPENDS-ON :: A list of defsystem dependencies.
+ :DEPENDS-ON :: A list of dependencies for the system.
+ :LOADED-SYSTEMS :: The list of systems loaded when the corresponding asd file
was loaded."
  (let* ((system (asdf:find-system system-name))
         (primary-system-name (asdf:primary-system-name system))
         (primary-system (asdf:find-system primary-system-name))

         (defsystem-deps (asdf:system-defsystem-depends-on system))
         (source-file (asdf:system-source-file primary-system))
         (depends-on (asdf:system-depends-on system))
         (description (asdf:system-description system))
         (license (asdf:system-license system))

         (primary-description (asdf:system-description primary-system))
         (primary-license (asdf:system-license primary-system)))

    (list :version (asdf:component-version system)
          :license (when (or (eql system primary-system)
                             (not (equal license primary-license)))
                     license)
          :description (when (or (eql system primary-system)
                                 (not (equal description primary-description)))
                         description)
          :source-file source-file
          :defsystem-depends-on defsystem-deps
          :depends-on depends-on
          :loaded-systems (gethash (namestring source-file) *directly-loaded-systems*))))


(defun lisp-file-wilden (pathname)
  "Make a wild pathname that matches all .lisp files under PATHNAME."
  (uiop:merge-pathnames*
   (uiop:merge-pathnames* (uiop:make-pathname* :name uiop:*wild*
                                               :type "lisp")
                          uiop:*wild-inferiors*)
   pathname))

(defun chase-primary-system-package-inferred-systems (system-name)
  "Given a system that is a package-inferred-system, return a list of all
package inferred systems with the same primary system."
  ;; Get the primary system, look at at its pathname and find any lisp files
  ;; that could be sub systems.
  (let* ((system (asdf:find-system system-name))
         (root-pathname (asdf:component-pathname system))
         (all-lisp-files (uiop:directory* (lisp-file-wilden root-pathname))))
    (remove nil
            (mapcar (lambda (pn)
                      (let* ((enough (enough-namestring pn root-pathname))
                             (system-name (uiop:strcat system-name "/"
                                                       ;; Trim the .lisp from the end.
                                                       (subseq enough 0 (- (length enough) 5)))))
                        ;; Make sure we can load a package inferred system from
                        ;; this file. (We've all done it: accidentally included
                        ;; an empty file or one with a mangled defpackage...)
                        (ignore-errors
                         (asdf:find-system system-name t)
                         system-name)))
                    all-lisp-files))))

(defun asd-file-systems (asd-pathname)
  "Given the pathname to an .asd file, return a list of systems (including
package-inferred-systems) defined by that file."
  (with-error-handling ()
    (let* ((systems (remove-if-not (lambda (system-name)
                                     (let* ((system (asdf:find-system system-name))
                                            (system-source-file (asdf:system-source-file system)))
                                       (uiop:pathname-equal asd-pathname system-source-file)))
                                   (asdf:registered-systems)))
           ;; If any of the systems are primary systems that are also package
           ;; inferred systems, we need to chase down all the secondary systems
           ;; coming from them...
           (primary-package-inferred-system-names
             (remove-if-not (lambda (system-name)
                              (and (equal system-name (asdf:primary-system-name system-name))
                                   (typep (asdf:find-system system-name) 'asdf:package-inferred-system)))
                            systems))
           (secondary-package-inferred-system-names
             (mapcan #'chase-primary-system-package-inferred-systems primary-package-inferred-system-names)))
      (remove-duplicates (append systems secondary-package-inferred-system-names) :test #'equal))))

(defun start-repl ()
  "Some CL implementations like to always print a prompt at their REPL. To avoid
needing to handle skipping this prompt when READ'ing the groveler's output, we
run our own REPL instead."
  (loop
    (let ((input (read)))
      (print (eval input))
      (terpri *standard-output*)
      (finish-output *standard-output*)
      (finish-output *error-output*))))
