;;;; Interface for spawning a new Lisp process as a child.
;;;;
;;;; This software is part of asdf-system-groveler. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:asdf-system-groveler/sublisp
    (:use #:cl
          #:lisp-invocation)
  (:export #:launch-sub-lisp
           #:stop-sub-lisp
           #:sub-lisp-alive-p
           #:sub-lisp-input
           #:sub-lisp-output))

(in-package #:asdf-system-groveler/sublisp)

(defclass sub-lisp-process ()
  ((impl
    :initarg :impl
    :reader sub-lisp-process-implementation-type)
   (process-info
    :initarg :process-info
    :reader sub-lisp-process-process-info)))

(defun sub-lisp-input (sub-lisp)
  (uiop:process-info-input (sub-lisp-process-process-info sub-lisp)))

(defun sub-lisp-output (sub-lisp)
  (uiop:process-info-output (sub-lisp-process-process-info sub-lisp)))

(defun launch-sub-lisp (implementation-type
                        &key
                          (rewrite-arguments-callback #'identity)
                          launch-program-args
                          lisp-path
                          lisp-args)
  (let* ((raw-lisp-invocation-arglist
           (unless (eql implementation-type :custom)
             (lisp-invocation-arglist :implementation-type implementation-type)))
         (arglist (append lisp-args (rest raw-lisp-invocation-arglist)))
         (lisp-path (or lisp-path (first raw-lisp-invocation-arglist))))
    (make-instance
     'sub-lisp-process
     :impl implementation-type
     :process-info (apply
                    #'uiop:launch-program
                    (funcall rewrite-arguments-callback (list* lisp-path arglist))
                    :input :stream
                    :output :stream
                    :error-output :interactive
                    launch-program-args))))

(defun sub-lisp-alive-p (sub-lisp)
  (uiop:process-alive-p (sub-lisp-process-process-info sub-lisp)))

(defun stop-sub-lisp (sub-lisp &key signal urgent)
  (if signal
      (uiop:terminate-process (sub-lisp-process-process-info sub-lisp) :urgent urgent)
      (let ((quit-form (quit-form :implementation-type (sub-lisp-process-implementation-type sub-lisp)))
            (input (sub-lisp-input sub-lisp)))
        (write-string quit-form input)
        (terpri input)))

  (uiop:wait-process (sub-lisp-process-process-info sub-lisp)))
