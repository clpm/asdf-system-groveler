;;;; asdf-system-groveler System Definition
;;;;
;;;; This software is part of asdf-system-groveler. See README.org for more
;;;; information. See LICENSE for license information.

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:asdf-system-groveler
  :version "0.0.1"
  :description "A library to grovel ASDF system information from .asd files."
  :license "BSD-2-Clause"
  :pathname "src/"
  :class :package-inferred-system
  :depends-on (#:asdf-system-groveler/asdf-system-groveler))
